"use strict";

const formOnPassword = document.querySelector(".password-form");
const btnSubmit = document.querySelector(".btn");
const validText = document.querySelector(".valid-text");

formOnPassword.addEventListener("click", showPassword);
btnSubmit.addEventListener("click", confirmPass);

function showPassword(e) {
  e.preventDefault();
  if (e.target.tagName === "LABEL" && e.currentTarget.tagName === "FORM") {
    return;
  }
  if (e.currentTarget === e.target) return;
  if (e.target.tagName === "INPUT") return;
  if (e.target.tagName === "BUTTON") return;
  if (e.target.tagName === "SPAN") return;

  if (e.target.dataset.eyeSlash) {
    e.target.dataset.eyeSlash = "";
    e.target.previousElementSibling.type = "password";
  } else {
    e.target.dataset.eyeSlash = "true";
    e.target.previousElementSibling.type = "text";
  }
  e.target.classList.toggle("fa-eye-slash");
}

const inputFirstPass = document.querySelector(".input_pass-first");
const inputSecondPass = document.querySelector(".input_pass-second");
inputFirstPass.addEventListener("focus", () => {
  validText.textContent = "";
});
inputSecondPass.addEventListener("focus", () => {
  validText.textContent = "";
});

function confirmPass(e) {
  if (inputFirstPass.value !== inputSecondPass.value) {
    validText.textContent = "Потрібно ввести однакові значення";
  } else if (inputFirstPass.value === "" && inputSecondPass.value === "") {
    validText.textContent = "Потрібно ввести значення";
  } else {
    alert("You are welcome;");
    inputFirstPass.value = "";
    inputSecondPass.value = "";
  }
}
