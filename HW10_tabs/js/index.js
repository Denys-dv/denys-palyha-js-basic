"use strict";

const ulTabs = document.querySelector(".tabs-js");
const ulTabsContant = document.querySelector(".tabs-content");

ulTabs.addEventListener("click", showTextTabs);

function showTextTabs(e) {
  const target = e.target;

  const listTabsContents = document.getElementById(`${target.dataset.tabs}`);
  setActivClass(target, listTabsContents);
}

function setActivClass(elemTabs, elemContact) {
  const activClassTab = ulTabs.querySelector(".active");
  const activClassContact = ulTabsContant.querySelector(".show-tabs-list");

  if (activClassTab || activClassContact) {
    activClassTab.classList.remove("active");
    activClassContact.classList.remove("show-tabs-list");
  }
  elemTabs.classList.add("active");
  elemContact.classList.add("show-tabs-list");
}
