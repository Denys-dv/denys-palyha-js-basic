"use strict";

const reworkDate = (dateUser) => {
  const daysUser = dateUser.slice(0, 2);
  const monthUser = dateUser.slice(3, 5);
  const yearsUser = dateUser.slice(6, 10);

  return `${yearsUser}.${monthUser}.${daysUser}`;
};

const createNewUser = () => {
  const userFirstName = prompt("Enter your first name");
  const userLastName = prompt("Enter your last name");
  const userBirthday = prompt("Your day birthday format dd.mm.yyyy");

  if (!userFirstName || !userLastName || !userBirthday) {
    return "cancel";
  }

  const newUser = {
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge() {
      const userBirthdayDate = new Date(reworkDate(userBirthday));
      const userBirthdayYear = userBirthdayDate.getFullYear();
      const userMonth = userBirthdayDate.getMonth() + 1;

      const dateNaw = new Date(Date.now());
      const dateNawYear = dateNaw.getFullYear();
      const mmNew = dateNaw.getMonth() + 1;

      if (mmNew < userMonth) {
        return `${dateNawYear - userBirthdayYear - 1} Years`;
      }
      return `${dateNawYear - userBirthdayYear} Years`;
    },

    getPassword() {
      const userDate = new Date(reworkDate(userBirthday));
      const userYear = userDate.getFullYear();
      return (
        this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + userYear
      );
    },

    setFirstName(newFName) {
      return Object.defineProperty(newUser, "firstName", {
        value: newFName,
        writable: false,
        configurable: true,
      });
    },
    setLastName(newLName) {
      return Object.defineProperty(newUser, "lastName", {
        value: newLName,
        writable: false,
        configurable: true,
      });
    },
  };
  Object.defineProperty(newUser, "firstName", {
    value: userFirstName,
    writable: false,
    configurable: true,
  });
  Object.defineProperty(newUser, "lastName", {
    value: userLastName,
    writable: false,
    configurable: true,
  });

  Object.defineProperty(newUser, "birthday", {
    value: userBirthday,
    writable: false,
    configurable: true,
  });

  return newUser;
};

const newUser = createNewUser();
console.log(newUser);

// const userGetLodin = newUser.getLogin();

// console.log(userGetLodin);

// console.log(newUser.setFirstName("Artem"));

// console.log(newUser.setLastName("Verba"));

// console.log(newUser.getLogin());

console.log(newUser.getAge());

console.log(newUser.getPassword());
