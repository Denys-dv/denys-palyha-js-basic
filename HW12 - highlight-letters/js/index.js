"use strict";

const wrapperBtn = document.querySelector(".btn-wrapper");

window.addEventListener("keydown", keyPressColorChange);

const keyCodePress = ["Enter", "KeyS", "KeyE", "KeyO", "KeyN", "KeyL", "KeyZ"];

function keyPressColorChange(e) {
  const findCode = keyCodePress.find((codeKey) => e.code === codeKey);

  const btnKeys = document.querySelector(`[data-code='${findCode}']`);
  getAndSetActive(btnKeys);
}

function getAndSetActive(btnKeys) {
  if (btnKeys !== null) {
    const activeClassBtnPressKey = wrapperBtn.querySelector(".active");

    if (activeClassBtnPressKey) {
      activeClassBtnPressKey.classList.remove("active");
    }
    btnKeys.classList.add("active");
  }
}
