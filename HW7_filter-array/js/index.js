"use strict";

const newArray = [
  43,
  "hello",
  false,
  { nameUser: "moty" },
  23,
  "world",
  true,
  { age: 20 },
  null,
];

const filterBy = (arr, typeOrFilter) => {
  return arr.filter((elem) => typeof elem !== typeOrFilter);
};

const allTypes = ["string", "number", "boolean", "object"];

allTypes.forEach((type) => console.log(filterBy(newArray, type)));

// ----------------------------------------------------------------------------
console.log(
  "----------------------------------------------------------------------------"
);

let arrFilter = [];

const filterBy2 = (arr, typeOrFilter) => {
  arr.forEach((elem) => {
    if (typeof elem !== typeOrFilter) {
      arrFilter.push(elem);
    }
  });
};

filterBy2(newArray, "object");

console.log(arrFilter);
