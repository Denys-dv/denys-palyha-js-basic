"use strict";

const elemNodeByParagraph = document.querySelectorAll("p");

elemNodeByParagraph.forEach((elem) =>
  elem.classList.add("elem-paragraph-style")
);

const elemOptionsList = document.getElementById("optionsList");
console.log(elemOptionsList);

console.log(elemOptionsList.parentElement);

for (let nodeElem of elemOptionsList.childNodes) {
  console.log(nodeElem);
}
const testParagraph = document.getElementById("testParagraph");
const text = (testParagraph.textContent = "This is a paragraph");
console.log(testParagraph);

const elemsHeader = document.querySelector(".main-header");

for (let elemHead of elemsHeader.children) {
  console.log(elemHead);
  elemHead.classList.add("nav-item");
}

const removeClass = document.querySelectorAll(".section-title");
removeClass.forEach((elem) => elem.classList.remove("section-title"));
