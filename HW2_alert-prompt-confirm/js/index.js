"use strict";

let userName = prompt("Введите имя!");

while (userName === "" || userName === " ") {
  userName = prompt("Введите имя!");
}

if (userName === null) {
  console.log("Отменнено пользователем!");
}

let userAge = prompt("Введите возраст!");

if (userAge === null) {
  console.log("Отменнено пользователем!");
}
let userAgeRew = Number(userAge);
let validNumber = Number.isNaN(userAgeRew);

while (validNumber || userAge === "" || userAge === " ") {
  userAge = prompt("Введите возраст!", userAge);
  userAgeRew = Number(userAge);
  validNumber = Number.isNaN(userAgeRew);
}

if (userAgeRew < 18 && userAgeRew != 0) {
  alert("You are not allowed to visit this website");
} else if (userAgeRew >= 18 && userAgeRew <= 22) {
  const reverseAnswer = confirm("Are you sure you want to continue?");
  const message = reverseAnswer
    ? `Welcome, ${userName}`
    : "You are not allowed to visit this website";
  alert(message);
} else {
  alert(`Welcome, ${userName}`);
}
