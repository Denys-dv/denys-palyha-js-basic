"use strict";

const btnDarckThem = document.querySelector(".install-darck-js");
const btnLightThem = document.querySelector(".install-light-js");
const bodyRef = document.querySelector("body");
const iceCreameImg = document.querySelector(".ice-cream-foto");
const btns = document.querySelectorAll(".btn-js");

btnDarckThem.addEventListener("click", setDarckThem);
btnLightThem.addEventListener("click", setLightThem);

const productTwo = "./img/product2.jpg";

setClassThemBody();

function setDarckThem() {
  document.body.classList.add("darck-them");
  document.body.classList.remove("light-them");
  //   additionalSettings
  btnLightThem.classList.remove("hide-btn");
  btnDarckThem.classList.add("hide-btn");
  btnLightThem.classList.add("light-btn-them-margin");
  // ______________________
  iceCreameImg.src = productTwo;

  btns.forEach((elem) => {
    elem.classList.add("btn_darck-them");
    elem.classList.remove("btn-primary");
  });

  localStorage.setItem("them", "darck-them");
  localStorage.setItem("them-image", "productTwo");
  localStorage.setItem("btn-them", "btn_darck-them");
}

function setLightThem() {
  document.body.classList.add("light-them");
  document.body.classList.remove("darck-them");
  //   additionalSettings
  btnLightThem.classList.add("hide-btn");
  btnDarckThem.classList.remove("hide-btn");
  btnLightThem.classList.remove("light-btn-them-margin");
  // ______________________

  iceCreameImg.src = "./img/product1.png";

  btns.forEach((elem) => {
    elem.classList.remove("btn_darck-them");
    elem.classList.add("btn-primary");
  });

  localStorage.setItem("them", "light-them");
  localStorage.setItem("them-image", "./img/product1.png");
  localStorage.setItem("btn-them", "btn-primary");
}

function setClassThemBody() {
  const getThem = localStorage.getItem("them");
  const getThemImage = localStorage.getItem("them-image");
  const getBtnThem = localStorage.getItem("btn-them");
  if (getThem === "light-them") {
    bodyRef.className = getThem;
    iceCreameImg.src = getThemImage;
    btns.forEach((elem) => (elem.className = getBtnThem));
  } else {
    bodyRef.className = "darck-them";
    iceCreameImg.src = productTwo;
    btns.forEach((elem) => elem.classList.add("btn_darck-them"));
  }
}
