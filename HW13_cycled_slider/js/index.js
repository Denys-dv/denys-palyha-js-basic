"use strict";

const imgColection = document.querySelectorAll(".image-to-show");
const imgWrapper = document.querySelector(".images-wrapper");
const btnStart = document.querySelector(".btn_start-js");
const btnStop = document.querySelector(".btn_stop-js");
const btnProceed = document.querySelector(".btn_proceed-js");
const timerSec = document.querySelector(".timer-sec");
const timerMs = document.querySelector(".timer-ms");

btnStart.addEventListener("click", startShowImages);
btnStop.addEventListener("click", stopCounterImg);
btnProceed.addEventListener("click", proceedShowImg);

let counterImg = 0;
let intervalId;
let timeoutIs;

function startShowImages() {
  intervalId = setInterval(() => {
    counterImg += 1;

    if (counterImg < imgColection.length) {
      imgColection[counterImg].classList.add("active");
    } else {
      counterImg = 0;

      imgColection[counterImg].classList.add("active");
    }
    show(counterImg);
    activeClass(counterImg);
  }, 3100);

  btnStart.remove();
  btnStop.classList.remove("hiden-btn");
  btnProceed.classList.remove("hiden-btn");
  btnStop.classList.add("show-btn");
  btnProceed.classList.add("show-btn");
  btnProceed.disabled = true;
}

function stopCounterImg() {
  clearInterval(intervalId);
  clearTimeout(timeoutIs);

  btnStop.disabled = true;
  btnProceed.disabled = false;
}

function proceedShowImg() {
  activeClass(counterImg);
  startShowImages();
  btnStop.disabled = false;
  btnProceed.disabled = true;
}

function activeClass(count) {
  const activeImg = imgWrapper.querySelector(".active");
  if (activeImg) {
    timeoutIs = setTimeout(() => {
      hide(count);
      imgColection[count].classList.remove("active");
    }, 3100);
  }
}

function show(counterImg) {
  $(`[data-img='${imgColection[counterImg].dataset.img}']`).fadeIn(500);
}
function hide(count) {
  $(`[data-img='${imgColection[count].dataset.img}']`).fadeOut(500);
}
