"use strict";

const userNumberOne = +prompt("Введите первое число!");
const userNumberTwo = +prompt("Введите второе число!");
const operationFn = prompt("Выберите оператор для вычисления +, -, *, /");

const calculationFn = (firstNum, secondNum, calcParams) => {
  switch (calcParams) {
    case "+":
      return firstNum + secondNum;
    case "-":
      return firstNum - secondNum;
    case "*":
      return firstNum * secondNum;
    case "/":
      return firstNum / secondNum;
    default:
      return "не найденно";
  }
};

console.log(calculationFn(userNumberOne, userNumberTwo, operationFn));
