"use strict";

const userNumber = +prompt("Введите число кратное 5");

const userRemNum = userNumber % 5;
const resultNum = userNumber - userRemNum;

for (let i = 5; i <= resultNum; i += 5) {
  console.log(i);
}

if (resultNum < 5) {
  console.log("Sorry, no numbers");
}
