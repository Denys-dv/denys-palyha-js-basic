"use strict";

document.body.insertAdjacentHTML("afterBegin", '<ul class="list"></ul>');

const getUnorderedList = document.querySelector(".list");

const arrElements = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arrElemNumStr = ["1", "2", "3", "sea", "user", 23];

const arrShowTwoElem = [
  "Kharkiv",
  "Kiev",
  ["Borispol", "Irpin"],
  "Odessa",
  "Lviv",
  "Dnieper",
];

getUnorderedList.insertAdjacentHTML(
  "afterBegin",
  '<ul class="list__item-next"></ul>'
);

const listItemNext = document.querySelector(".list__item-next");

const showUnorderedList = (itemArr, htmlElem = document.body) => {
  const newArrElements = itemArr.map((item) => {
    if (Array.isArray(item)) {
      showUnorderedList(item, listItemNext);
    } else {
      return `<li class="item-list">${item}</li>`;
    }
  });

  htmlElem.insertAdjacentHTML("afterBegin", newArrElements.join(""));
};

showUnorderedList(arrShowTwoElem, getUnorderedList);

// const countParagraph = document.createElement("p");
// countParagraph.className = "count-paragraph";
// document.body.append(countParagraph);

// const removeElem = (htmlElem) => {
//   htmlElem.remove();
// };

// let counterSum = 4;

// const countdown = setInterval(() => {
//   counterSum -= 1;
//   countParagraph.innerHTML = counterSum;
// }, 1000);

// setTimeout(() => {
//   removeElem(getUnorderedList);
//   removeElem(countParagraph);
//   clearInterval(countdown);
// }, 3500);
