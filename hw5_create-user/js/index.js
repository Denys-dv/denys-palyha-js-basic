"use strict";

const createNewUser = () => {
  const userFirstName = prompt("Enter your first name");
  const userLastName = prompt("Enter your last name");

  if (!userFirstName || !userLastName) {
    return "cancel";
  }

  const newUser = {
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },

    setFirstName(newFName) {
      return Object.defineProperty(newUser, "firstName", {
        value: newFName,
        writable: false,
        configurable: true,
      });
    },
    setLastName(newLName) {
      return Object.defineProperty(newUser, "lastName", {
        value: newLName,
        writable: false,
        configurable: true,
      });
    },
  };

  Object.defineProperty(newUser, "firstName", {
    value: userFirstName,
    writable: false,
    configurable: true,
  });

  Object.defineProperty(newUser, "lastName", {
    value: userLastName,
    writable: false,
    configurable: true,
  });

  return newUser;
};

const newUser = createNewUser();
console.log(newUser);

const userGetLodin = newUser.getLogin();

console.log(userGetLodin);

console.log(newUser.setFirstName("Artem"));

console.log(newUser.setLastName("Verba"));

console.log(newUser.getLogin());
